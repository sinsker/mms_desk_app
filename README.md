# IVY 웹로그 분석 솔루션 

**Web 기반 데스크톱 플랫폼 애플리케이션**

This is a minimal Electron application based on the [Quick Start Guide](http://electron.atom.io/docs/tutorial/quick-start) within the Electron documentation.


## To use

node.js 기반 electron 사용 

```bash
# Clone this repository
git clone https://github.com/**
# Go into the repository
cd pkg
# Install dependencies
npm i
# Run the app
npm run start
# Build the app [from window]
npm run window 
```
