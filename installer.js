const  electronInstaller  = require('electron-installer-squirrel-windows');
/*

resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: './dist/ivy-win32-x64',
    outputDirectory: './dist/installer-win32-x64',
    exe: 'Ivy.exe',
});

resultPromise.then(function () {
    console.log("It worked!");
}, function (e) {
    console.log('No dice: ' + e.message);
});
*/


electronInstaller({
    name : 'IVY-MarketingManagementSystem',
    path: './dist/IVY-win32-x64',
    out: './dist/installer',
    authors: 'IVY System',
    owners: 'Fabbit',
    version: '1.0',
    exe: 'Ivy.exe',
    appDirectory: './dist/IVY-win32-x64',
    overwrite: true,
    iconUrl:'http://ivy.hackers.com/favicon.ico',
    setup_icon: './resources/icon/setup.ico'
}, function done (e) {
    console.log('Build success !!');
});